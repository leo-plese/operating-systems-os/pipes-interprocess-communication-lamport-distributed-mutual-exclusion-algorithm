Pipes Interprocess Communication Lamport's Distributed Mutual Exclusion Algorithm. Implemented in C.

My lab assignment in Advanced Operating Systems, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2021
