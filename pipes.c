#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <errno.h>
#define MAX_MSG_LEN 12
#define FIELD_PID_LEN 6
#define FIELD_CI_LEN 5

// struktura zahtjeva koje pojedini proces stavlja u svoj red zahtjeva
struct req {
	int proc_id;
	int ci;
};

// zadana struktura zapisa u bazi podataka (ID procesa, lokalni logicki sat, broj ulaza u KO) 
struct db_entry {
	int proc_id;
	int ci;
	int n_ko;
};

// baza podataka
struct db_entry *database;

// broj procesa koji se natjecu za ulaz u KO (init u main-u)
int N;


int sleep_ns_min = 100, sleep_ns_max = 2000;

// funkcija koja sortira uzlazno zahtjeve u redu zahtjeva procesa: 1. po ci, 2. po proc_id (ako su ci jednaki)
int f_comp(const void *a, const void *b) {
	struct req *req1 = (struct req*) a;
	struct req *req2 = (struct req*) b;
	if (req1->ci < req2->ci)
		return -1;
	if (req1->ci > req2->ci)
		return 1;
	// req1->ci == req2->ci
	if (req1->proc_id < req2->proc_id)
		return -1;
	return 1;
}

// dodaje zahtjev u red zahtjeva procesa
void add_request_to_msgq(int *qlen, struct req *my_msgq, int child_pid, int ci) {
	struct req new_req;

	new_req.proc_id = child_pid;
	new_req.ci = ci;

	my_msgq[(*qlen)++] = new_req;

	qsort(my_msgq, *qlen, sizeof(struct req), f_comp);

}

// uklanja zahtjev s pocetka reda zahtjeva procesa
void remove_first_element(int *qlen,  struct req **my_msgq) {
	if (*qlen == 0)
		return;

	(*my_msgq) = (*my_msgq)+1;
	*qlen = *qlen - 1;

}

// uklanja odredeni zahtjev(pi, ci) iz reda zahtjeva procesa)
void remove_request(int *qlen, struct req **my_msgq, int ci, int pi) {

	if (*qlen == 0)
		return;

	int index_rem = -1;
	for (int i=0; i<*qlen; i++) {

		if ( (*my_msgq)[i].ci == ci &&  (*my_msgq)[i].proc_id == pi) {
			index_rem = i;
			break;
		}
		

	}

	// element not found
	if (index_rem == -1)
		return;

	for (int i=index_rem; i<(*qlen - 1); i++) {
		(*my_msgq)[i] = (*my_msgq)[i+1];

	}

	*qlen = *qlen - 1;


}

// kriticni odsjecak
void ko(int proc_rbr, int ci, int ith_time_ko) {
	printf("---- IN [%d]: PROCES %d ----\n", ith_time_ko, proc_rbr);
	// 1. korak
	database[proc_rbr].ci = ci;
	database[proc_rbr].n_ko++;

	// 2. korak
	printf("DB:\n");
	for (int i=0; i<N; i++) {
		printf("p%d: %d | %d | %d\n", i, database[i].proc_id, database[i].ci, database[i].n_ko);
	}

	// 3. korak
	int X_ns = (rand() % (sleep_ns_max + 1 - sleep_ns_min) + sleep_ns_min) * 1000000L;
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec = X_ns;
	nanosleep(&ts, NULL);


	printf("---- OUT [%d]: PROCES %d ----\n", ith_time_ko, proc_rbr);

}

// funkcija koju obavlja svaki od N procesa
void do_child(int fds[10][2], int proc_rbr) {
	// vlastiti pipe
	int *my_pipe = fds[proc_rbr];


	printf("************ CHILD %d.....\n", proc_rbr);
	// zatvoriti kraj svog cjevovoda za pisanje (slanje poruka)
	close(my_pipe[1]);
	// zatvoriti kraj ostalih N-1 cjevovoda za citanje (primanje poruka)
	for (int i=0; i<N; i++) {
		if (i==proc_rbr)
			continue;
		int *other_pipe = fds[i];
		close(other_pipe[0]);
	}

	char child_pid_buf[FIELD_PID_LEN+1];
	sprintf(child_pid_buf, "%06d", proc_rbr);

	int ci = 0;

	// VAZNO : struktura poruka koje saljem u pipeove: XYYYYYYZZZZZ
	// X = "0" (zahtjev), "1" (odgovor), "2" (izlazak)
	// YYYYYY - 6 charactera (PID=rbr procesa nadupunjen s nulama ispred npr. za PID = 3 ce biti "000003") -> sa sprintf pretvori se u int
	// ZZZZZ - 5 charactera (ci (vremenska oznaka) nadopunjena s nulama ispred) -> sa sprintf pretvori se u int

	// vlastiti prioritetni red poruka-zahtjeva
	int qlen;
	qlen = 0;
	struct req *my_msgq = (struct req *) malloc(5*N*sizeof(struct req));

	char ti_buf[FIELD_CI_LEN+1];
	// 5x ulazi u KO
	for (int i=0; i<5; i++) {
		printf("%d : i = %d ############################\n", proc_rbr, i);
	
		char msg_buf[MAX_MSG_LEN+1] = "";
		msg_buf[0] = '0';	// 0 - zahtjev, 1 -odgovor, 2 - izlazak

		sprintf(ti_buf, "%05d", ci);
	
		strcat(msg_buf, child_pid_buf);
		strcat(msg_buf, ti_buf);

		msg_buf[MAX_MSG_LEN] = '\0';
	
		// 1. korak
		// zapisi Zahtjev u svoj red poruka
		add_request_to_msgq(&qlen, my_msgq, proc_rbr, ci);
	
		int my_last_req_ci = ci;


		// salje Zahtjev svim ostalim procesima
		for (int j=0; j<N; j++) {
			if (proc_rbr == j) continue;
			
			write(fds[j][1], msg_buf, MAX_MSG_LEN+1);
			printf("%d: SALJE Zahtjev(%d, %d) -> %d\n", proc_rbr, proc_rbr, ci, j);
		}

		// 3. korak (ceka na ulaz u KO)- prima poruke 0 1 2
		int n_odgovora = 0;
		// ceka za ulazak u KO dok nije dobio odgovor od svih ostalih N-1 procesa i dok nije njegov zahtjev na pocetku reda zahtjeva
		while (!(n_odgovora == N-1 && my_msgq[0].proc_id == proc_rbr)) {
			char msg_buf_r[MAX_MSG_LEN+1] = "";
	
			// procita poruku
			while (read(my_pipe[0], msg_buf_r, MAX_MSG_LEN+1)<=MAX_MSG_LEN);

			// izvuce pi i ci iz poruke
			char *other_pid_str =  (char *) malloc((FIELD_PID_LEN+1)*sizeof(char));
			char *other_ci_str =  (char *) malloc((FIELD_CI_LEN+1)*sizeof(char));
			memcpy(other_pid_str, &msg_buf_r[1], &msg_buf_r[FIELD_PID_LEN+1]-&msg_buf_r[1]);
			memcpy(other_ci_str, &msg_buf_r[FIELD_PID_LEN+1], &msg_buf_r[MAX_MSG_LEN]-&msg_buf_r[FIELD_PID_LEN+1]);

			int other_pid, other_ci;
			sscanf(other_pid_str, "%06d", &other_pid);
			sscanf(other_ci_str, "%05d", &other_ci);

			// ovisno o tipu poruke (0 - zahtjev, 1 - odgovor, 2 - izlazak) obradi
			switch (msg_buf_r[0]) {
				case '0':	// 2. korak
					printf("%d: PRIMA Zahtjev(%d, %d)\n", proc_rbr, other_pid, other_ci);
					// update logicki sat: ci = max(ci, T(j))+1
					ci = ci >= other_ci ? ci+1 : other_ci+1;

					// doda zahtjev (j, T(j)) u svoj red zahtjeva
					add_request_to_msgq(&qlen, my_msgq, other_pid, other_ci);

					// salje Odgovor procesu j (tip 1 poruke)
					char msg_buf_w[MAX_MSG_LEN+1] = "";

					msg_buf_w[0] = '1';

					sprintf(child_pid_buf, "%06d", other_pid);
					sprintf(ti_buf, "%05d", ci);
				
					strcat(msg_buf_w, child_pid_buf);
					strcat(msg_buf_w, ti_buf);

					msg_buf_w[MAX_MSG_LEN] = '\0';

			
					write(fds[other_pid][1], msg_buf_w, MAX_MSG_LEN+1);
					printf("%d: SALJE Odgovor(%d, %d)\n", proc_rbr, other_pid, ci);
				
					break;
				case '1':	// evidencija br odgovora - vazna za ulaz u KO
					printf("%d: PRIMA Odgovor(%d, %d)\n", proc_rbr, other_pid, other_ci);
					n_odgovora++;
					break;
				case '2':	// 5. korak
					printf("%d: PRIMA Izlazak(%d, %d)\n", proc_rbr, other_pid, other_ci);
					remove_request(&qlen, &my_msgq, other_ci, other_pid);

					break;
				default:
					perror("greska kod obrade primljene poruke");
	
			}


		}

		// KO (i. put)
		ko(proc_rbr, ci, i+1);

		// 4. korak
		// odstrani svoj zahtjev (1. na redu) iz reda zahtjeva
		remove_first_element(&qlen, &my_msgq);

		// salje Izlazak svim ostalim procesima
		char msg_buf_e[MAX_MSG_LEN+1] = "";
		msg_buf_e[0] = '2';

		sprintf(child_pid_buf, "%06d", proc_rbr);
		sprintf(ti_buf, "%05d", my_last_req_ci);
				
		strcat(msg_buf_e, child_pid_buf);
		strcat(msg_buf_e, ti_buf);

		msg_buf_e[MAX_MSG_LEN] = '\0';
		

		for (int j=0; j<N; j++) {
			if (proc_rbr == j) continue;
			
			write(fds[j][1], msg_buf_e, MAX_MSG_LEN+1);
			printf("%d: SALJE Izlazak(%d, %d) -> %d\n", proc_rbr, proc_rbr, my_last_req_ci, j);
		}
		
		

	}

	// iako je zavrsio 5x ulazak u KO, mogu postojati drugi procesi koji jos nisu zavrsili
	// -> treba i dalje citati i obradivati poruke (isto kao i prije)
	while (qlen > 0) {

			char msg_buf_r_end[MAX_MSG_LEN+1] = "";

			while (read(my_pipe[0], msg_buf_r_end, MAX_MSG_LEN+1)<=MAX_MSG_LEN);
		

			char *other_pid_str =  (char *) malloc((FIELD_PID_LEN+1)*sizeof(char));
			char *other_ci_str =  (char *) malloc((FIELD_CI_LEN+1)*sizeof(char));
			memcpy(other_pid_str, &msg_buf_r_end[1], &msg_buf_r_end[FIELD_PID_LEN+1]-&msg_buf_r_end[1]);
			memcpy(other_ci_str, &msg_buf_r_end[FIELD_PID_LEN+1], &msg_buf_r_end[MAX_MSG_LEN]-&msg_buf_r_end[FIELD_PID_LEN+1]);

			int other_pid, other_ci;
			sscanf(other_pid_str, "%06d", &other_pid);
			sscanf(other_ci_str, "%05d", &other_ci);

			switch (msg_buf_r_end[0]) {
				case '0':	// 2. korak
					printf("%d: PRIMA Zahtjev(%d, %d)\n", proc_rbr, other_pid, other_ci);
					// update logicki sat: ci = max(ci, T(j))+1
					ci = ci >= other_ci ? ci+1 : other_ci+1;

					// doda zahtjev (j, T(j)) u svoj red zahtjeva
					add_request_to_msgq(&qlen, my_msgq, other_pid, other_ci);

					// salje Odgovor procesu j
					char msg_buf_w_end[MAX_MSG_LEN+1] = "";
					msg_buf_w_end[0] = '1';

					sprintf(child_pid_buf, "%06d", other_pid);
					sprintf(ti_buf, "%05d", ci);
				
					strcat(msg_buf_w_end, child_pid_buf);
					strcat(msg_buf_w_end, ti_buf);

					msg_buf_w_end[MAX_MSG_LEN] = '\0';

			
					write(fds[other_pid][1], msg_buf_w_end, MAX_MSG_LEN+1);
					printf("%d: SALJE Odgovor(%d, %d)\n", proc_rbr, other_pid, ci);
				
					break;
				case '1':	// evidencija br odgovora - ovdje nepotrebna, ne bi trebao vise primati odgovore
					printf("%d: ??? PRIMA Odgovor(%d, %d)\n", proc_rbr, other_pid, other_ci);
					break;
				case '2':	// 5. korak
					printf("%d: PRIMA Izlazak(%d, %d)\n", proc_rbr, other_pid, other_ci);
					remove_request(&qlen, &my_msgq, other_ci, other_pid);
					break;
				default:
					perror("greska kod obrade primljene poruke");
	
			}
		

	}
	printf("************************* P %d GOTOV (msgq len = %d) *************************\n", proc_rbr, qlen);


}

int main(int argc, char **argv) {
	// odrediti broj procesa N = 3, ..., 10 koji ce htjeti ulaziti u KO
	if (argc != 2) {
		printf("Treba unijeti N\n");
		return -1;
	}
	
	char *N_str = argv[1];
	char *buf_reg;
	N = strtol(N_str, &buf_reg, 10);
	if (N < 3 || N > 10) {
		printf("N treba biti cijeli broj iz [3, 10]\n");
		return -1;
	}
	
	printf("uneseno: N = %d\n", N);

	

	printf("N = <%d>\n", N);

	// baza podataka je u dijeljenoj memoriji
	int shared_mem_id = shmget(IPC_PRIVATE, sizeof(database)*N, 0600);
	database = (struct db_entry*) shmat(shared_mem_id, NULL, 0);


	int fd[2];
	int fds[10][2];
	// kreirati N pipeova
	for (int i=0; i<N; i++) {
		if (pipe(fd) == -1)
			exit(1);
		printf("kreiran pipe\n");
		fds[i][0] = fd[0];
		fds[i][1] = fd[1];

		
	}



	// kreirati N procesa
	for (int i=0; i<N; i++) {
		

		switch (fork()) {
			case -1:
				printf("Ne moze kreirati novi proces\n");
				break;
			case 0:
				printf("Kreira novi proces %d\n", i);
				srand(10);

				struct db_entry p_db_ent;
				p_db_ent.proc_id = i;
				p_db_ent.ci = 0;
				p_db_ent.n_ko = 0;
				database[i] = p_db_ent;

				
				
				do_child(fds, i);
	
				exit(1);
				
		}

		
	}

	// pricekati N procesa da zavrse
	for (int i=0; i<N; i++) {
		printf("Cekam...\n");
		wait(NULL);
	}

	// pobrinuti se za ciscenje zajednickog meduspremnika
	shmdt(database);
	shmctl(shared_mem_id, IPC_RMID, NULL);




	return 0;
}
